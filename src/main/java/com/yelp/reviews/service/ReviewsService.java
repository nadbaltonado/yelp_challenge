package com.yelp.reviews.service;

import java.io.IOException;
import java.util.List;

import com.yelp.reviews.model.ReviewModel;

public interface ReviewsService {
	
	public List<ReviewModel> getReviews(String url) throws Exception;

}
