package com.yelp.reviews.service.impl;

import com.yelp.reviews.model.FaceEmotionModel;
import com.yelp.reviews.service.ImageService;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Service("imageService")
public class ImageServiceImpl implements ImageService {
	
	private static final String TARGET_URL = "https://vision.googleapis.com/v1/images:annotate?";
	private static final String API_KEY = "key=AIzaSyA2wWdOsIpye-jmvFtZz6cGlFmdo8WmG3E";

	@Override
	public FaceEmotionModel getImageEmotion(String imageUrl) throws Exception {
		
		FaceEmotionModel emotion = new FaceEmotionModel();
		
		URL serverUrl = new URL(TARGET_URL + API_KEY);
		URLConnection urlConnection = serverUrl.openConnection();
		HttpURLConnection httpConnection = (HttpURLConnection)urlConnection;
		
		httpConnection.setRequestMethod("POST");
		httpConnection.setRequestProperty("Content-Type", "application/json");
		httpConnection.setDoOutput(true);
		
		//Data portion of the request
		BufferedWriter httpRequestBodyWriter = new BufferedWriter(new OutputStreamWriter(httpConnection.getOutputStream()));
		httpRequestBodyWriter.write
			 ("{\"requests\":  [{ \"features\":  [ {\"type\": \"FACE_DETECTION\""
			 +"}], \"image\": {\"source\": { \"imageUri\":"
			 +" \""
			 + imageUrl
			 + "\"}}}]}");				
		httpRequestBodyWriter.close();
		
		String response = httpConnection.getResponseMessage();		
		
		if (httpConnection.getInputStream() == null) {
			   System.out.println("No stream");
			   //return;
			}

			Scanner httpResponseScanner = new Scanner (httpConnection.getInputStream());
			String resp = "";
			while (httpResponseScanner.hasNext()) {
			   String line = httpResponseScanner.nextLine();
			   resp += line;
			   //System.out.println(line);  //  alternatively, print the line of response
			}
			httpResponseScanner.close();		
		
			JSONObject myObject = new JSONObject(resp);			
			JSONArray respArray = myObject.getJSONArray("responses");			
			JSONObject faceAnnotate = respArray.getJSONObject(0);	
			
			if (faceAnnotate != null && faceAnnotate.length()>0) {
				JSONArray faceArray = faceAnnotate.getJSONArray("faceAnnotations");
				if (faceArray != null && faceArray.length()>0) {
					JSONObject obj1 =  faceArray.getJSONObject(0);			
					emotion.setAngerLikelihood( obj1.getString("angerLikelihood"));
					emotion.setBlurredLikelihood(obj1.getString("blurredLikelihood"));
					emotion.setHeadwearLikelihood(obj1.getString("headwearLikelihood"));
					emotion.setJoyLikelihood(obj1.getString("joyLikelihood"));
					emotion.setSorrowLikelihood(obj1.getString("sorrowLikelihood"));
					emotion.setSurpriseLikelihood(obj1.getString("surpriseLikelihood"));
					emotion.setUnderExposedLikelihood(obj1.getString("underExposedLikelihood"));				
				}						
			}
			
			

		return emotion;
	}

}
