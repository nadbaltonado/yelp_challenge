package com.yelp.reviews.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yelp.reviews.model.FaceEmotionModel;
import com.yelp.reviews.model.ReviewModel;
import com.yelp.reviews.model.ReviewerModel;
import com.yelp.reviews.service.ReviewsService;
import com.yelp.reviews.service.ImageService;

@Service("reviewService")
public class ReviewsServiceImpl implements ReviewsService {
	
	@Autowired	
	private ImageService imageService;		
	
	@Override
	public List<ReviewModel> getReviews(String url) throws Exception {
		List<ReviewModel> listReviews = new ArrayList<ReviewModel>();		
		Document doc = Jsoup.connect(url)
						.userAgent("Mozilla (Windows NT 6.1) AppleWebKit (KHTML, like Gecko) Chrome/67.0.3396.99 Safari")
						.get();

		Elements reviews = doc.select("div.review--with-sidebar");
		
		int i=0; 
		for (Element reviewList:reviews) {
			i++;
			
			if (i>1) { //Start on second element of <li>. The first one is blank and for new reviews.
				//System.out.println("i: "+i);
				ReviewModel model = new ReviewModel();
				
				//Review-Content
				Element content = reviewList.getElementsByClass("review-content").get(0);				
				model.setReviews(content.getElementsByTag("p").first().text());
				model.setDate(content.getElementsByTag("span").first().text());		
				
				//Reviewer Details: ypassport media-block				
				ReviewerModel reviewer = new ReviewerModel();
				Element profile = reviewList.getElementsByClass("ypassport media-block").first();
				reviewer.setName(profile.getElementById("dropdown_user-name").text());
				reviewer.setAddress(profile.getElementsByClass("user-location").text());
				Element image = profile.select("img").first();								
				String images = image.absUrl("srcset");
		        reviewer.setImagePath(getLargestImg(images));
		        if (reviewer.getImagePath() != null && !reviewer.getImagePath().equals("")) {
		        	//System.out.println("reviewer.getImagePath(): "+reviewer.getImagePath());
		        	reviewer.setFacialEmotion(this.getFaceDetection(reviewer.getImagePath()));
		        }		       
				model.setReviewer(reviewer);
				
				listReviews.add(model);			
			}
			
		} //end of for loop
			
		
		return listReviews;
	}
	
	
	/**
	 * Get largest image
	 * **/	
	private String getLargestImg(String images) {
		String largestImage="";
        String [] arrOfImages = images.split(",");
        int indexLargeImg = arrOfImages.length -2;
        for (int j=0; j<=indexLargeImg; j++) {        	
        	if (j==indexLargeImg) largestImage=arrOfImages[j];
        }	
        String [] arrOfStr = largestImage.split(" ");
        largestImage = arrOfStr[0];


		return largestImage;
	}
	
	private FaceEmotionModel getFaceDetection(String imagePath) throws Exception {		
		return imageService.getImageEmotion(imagePath);
	}	
	

}
