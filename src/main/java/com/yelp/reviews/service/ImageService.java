package com.yelp.reviews.service;

import com.yelp.reviews.model.FaceEmotionModel;

public interface ImageService {
	
	public FaceEmotionModel getImageEmotion(String imageUrl) throws Exception;
	
}
