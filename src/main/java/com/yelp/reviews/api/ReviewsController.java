package com.yelp.reviews.api;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.yelp.reviews.service.ReviewsService;
import com.yelp.reviews.model.ApiResponse;


@RestController
public class ReviewsController {
	
	@Autowired	
	private ReviewsService reviewService;	

	private static StringBuilder YELP_URL= new StringBuilder("https://www.yelp.com/biz/");
	
	   @RequestMapping("/yelp/reviews")
	    public ApiResponse getStatus(@RequestParam(value="restaurant", defaultValue="milwaukee-ale-house-milwaukee") String restaurant) {
		   
		   ApiResponse response = new ApiResponse();
		   restaurant = YELP_URL.append(restaurant).toString();		  
		   
		   try {
			
			   response.setListReviews(reviewService.getReviews(restaurant));
		       response.setApiStatus("SUCCESS");
		       response.setMessage("Message here"); 
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();			
		}
		   
	       return response;
	       
	    }	
	   
	    @RequestMapping("/")
	    public String index() {
	        return "Welcome to Yelp Rewiews!";
	    }		
}
