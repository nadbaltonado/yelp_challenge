package com.yelp.reviews.model;

import java.util.List;

public class ApiResponse {
	
	private String apiStatus;
	private String message;
	private List<ReviewModel> listReviews;
	
	public String getApiStatus() {
		return apiStatus;
	}
	public void setApiStatus(String apiStatus) {
		this.apiStatus = apiStatus;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<ReviewModel> getListReviews() {
		return listReviews;
	}
	public void setListReviews(List<ReviewModel> listReviews) {
		this.listReviews = listReviews;
	}
	

}
