package com.yelp.reviews.model;

import com.yelp.reviews.model.ReviewerModel;

public class ReviewModel {
	
	private ReviewerModel reviewer;
	private String date;	
	private String reviews;
	
	
	public ReviewerModel getReviewer() {
		return reviewer;
	}
	public void setReviewer(ReviewerModel reviewer) {
		this.reviewer = reviewer;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}	
	public String getReviews() {
		return reviews;
	}
	public void setReviews(String reviews) {
		this.reviews = reviews;
	}

	
	

}
