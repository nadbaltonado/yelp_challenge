package com.yelp.reviews.model;

public class ReviewerModel {
	
	private String name;
	private String imagePath;
	private String address;
	private FaceEmotionModel facialEmotion;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public FaceEmotionModel getFacialEmotion() {
		return facialEmotion;
	}
	public void setFacialEmotion(FaceEmotionModel facialEmotion) {
		this.facialEmotion = facialEmotion;
	}
	
	

}
