BDO SpringBoot

Command to run:
java -jar target/nina-yelp-0.1.0.jar

mvn package && java -jar target/nina-yelp-0.1.0.jar

-------------------------------
Sample Request Format:
http://localhost:8080/yelp/reviews
http://localhost:8080/yelp/reviews?restaurant=milwaukee-ale-house-milwaukee

Sample Response:
{
    "apiStatus": "SUCCESS",
    "message": "Message here",
    "listReviews": [
        {
            "reviewer": {
                "name": "Kristen V.",
                "imagePath": "https://s3-media4.fl.yelpcdn.com/photo/CARNTh10wVGArD2K2FNSyw/180s.jpg",
                "address": "Forest Park, IL",
                "facialEmotion": {
                    "joyLikelihood": "VERY_LIKELY",
                    "sorrowLikelihood": "VERY_UNLIKELY",
                    "angerLikelihood": "VERY_UNLIKELY",
                    "surpriseLikelihood": "VERY_UNLIKELY",
                    "underExposedLikelihood": "VERY_UNLIKELY",
                    "blurredLikelihood": "VERY_UNLIKELY",
                    "headwearLikelihood": "VERY_UNLIKELY"
                }
            },
            "date": "6/6/2018",
            "reviews": "What a nice spot on Water Street! The place is clean and gigantic! We stopped in for a quick lunch before a wedding, and if it had been warmer, we would have sat on the deck overlooking the river. Like any Chicagoan, we ordered the cheese curds and fries cause, you know, Milwaukee. They were hot, crisp, and very tasty, with a nice beer cheese aioli to dip them in. We also had the hummus plate, so we could feel less guilty about said cheese curds. The beers are so interesting, and the flight is a great deal. I loved the horchata cream ale, and the blueberry tea ale was an interesting take on this popular summertime beer. Husband loved all the different IPAs. The bartender was very sweet, and gave us good tips on where to go for frozen custard. Nice place!"
        },
        {
            "reviewer": {
                "name": "Jasper P.",
                "imagePath": "https://s3-media2.fl.yelpcdn.com/photo/ur50pnO5rAF6OCLtSumwbg/180s.jpg",
                "address": "Milwaukee, WI",
                "facialEmotion": {
                    "joyLikelihood": "VERY_LIKELY",
                    "sorrowLikelihood": "VERY_UNLIKELY",
                    "angerLikelihood": "VERY_UNLIKELY",
                    "surpriseLikelihood": "VERY_UNLIKELY",
                    "underExposedLikelihood": "VERY_UNLIKELY",
                    "blurredLikelihood": "VERY_UNLIKELY",
                    "headwearLikelihood": "VERY_UNLIKELY"
                }
            },
            "date": "3/18/2018",
            "reviews": "Another great restaurant in Milwaukee! While here, I got a burger, along with cheese curds appetizer, and a few drinks. The burger tasted fantastic, and the Louie's Demise barbeque sauce was very good! The rest of the food ordered looked incredible as well. The cheese curds were very good, though there weren't very many, and were mixed in with more fries than curds. The beer was very good (of course), and as I went during happy hour, not very pricy as well. Over all, it was a great restaurant with a fun atmosphere, and good food, however, the price did add up. I expected it to, of course, with the amount that I got, and where it was located."
        },
        {
            "reviewer": {
                "name": "R Jimmie L.",
                "imagePath": "https://s3-media1.fl.yelpcdn.com/photo/FP_bmxihUSbkrETlOP5Bag/180s.jpg",
                "address": "Chicago, IL",
                "facialEmotion": {
                    "joyLikelihood": "VERY_LIKELY",
                    "sorrowLikelihood": "VERY_UNLIKELY",
                    "angerLikelihood": "VERY_UNLIKELY",
                    "surpriseLikelihood": "VERY_UNLIKELY",
                    "underExposedLikelihood": "VERY_UNLIKELY",
                    "blurredLikelihood": "VERY_UNLIKELY",
                    "headwearLikelihood": "VERY_UNLIKELY"
                }
            },
            "date": "3/17/2018",
            "reviews": "We came in on a Friday night after walking through the Third Ward. A chalkboard on the sidewalk advertised $3 MKE brewing cans and $4 Captain. We were promptly seated in the dining area followed by attentive greeting from our server. We looked over the beer menu at the table which also advertised the $3 special. We made our selection and then was told by the server that this was only valid in the bar. This was quite disappointing considering the sidewalk chalkboard and table beer menu didn't mention this. We stayed seated in the dining area and proceeded to order. I had fish and chips which were decent quality. They were friend in beer (house brew) batter and for an extra $2 came with an extra piece. The portion would be quite underwhelming without the extra order. The bottom side of the fish was soggy on a few pieces. Slaw was bland but fries were excellent. We also had a salmon salad which was surprisingly better than the fish and chips. Salmon was grilled with visible marks and dressing was tasty. Overall the food was decent but it was difficult to overcome the false advertisement. We never saw a manager and the server just walked away from the table when we mentioned it was disappointing this is falsely advertised."
        },
        {
            "reviewer": {
                "name": "Becky J.",
                "imagePath": "https://s3-media3.fl.yelpcdn.com/photo/UTCZXFy4To8yejjJKWSnEA/180s.jpg",
                "address": "Milwaukee, WI",
                "facialEmotion": {
                    "joyLikelihood": "VERY_LIKELY",
                    "sorrowLikelihood": "VERY_UNLIKELY",
                    "angerLikelihood": "VERY_UNLIKELY",
                    "surpriseLikelihood": "VERY_UNLIKELY",
                    "underExposedLikelihood": "VERY_UNLIKELY",
                    "blurredLikelihood": "VERY_UNLIKELY",
                    "headwearLikelihood": "VERY_UNLIKELY"
                }
            },
            "date": "5/8/2018",
            "reviews": "We decided to stop in here for dinner on a Friday night with a group of four. We opted to sit inside since the outside seating was a wait, and were seated immediately. The service was fine, but kind of slow. We tried the mango tango martinis, and they were not enjoyable. Others with me got the margarita and a beer and enjoyed both of those. I went with the chicken pot pie for my entree and it was AMAZING. Comfort food at its finest. Couldn't have asked for better. We also got the caprese flatbread and that was also really good. Overall a pretty good experience. Really nice location right on the river walk."
        },
        {
            "reviewer": {
                "name": "Emily S.",
                "imagePath": "https://s3-media1.fl.yelpcdn.com/photo/amQ9UzGHTq3trPe99Yogrg/180s.jpg",
                "address": "Milwaukee, WI",
                "facialEmotion": {
                    "joyLikelihood": "VERY_LIKELY",
                    "sorrowLikelihood": "VERY_UNLIKELY",
                    "angerLikelihood": "VERY_UNLIKELY",
                    "surpriseLikelihood": "VERY_UNLIKELY",
                    "underExposedLikelihood": "VERY_UNLIKELY",
                    "blurredLikelihood": "VERY_UNLIKELY",
                    "headwearLikelihood": "POSSIBLE"
                }
            },
            "date": "4/26/2018",
            "reviews": "Excellent beer, food, and riverside dining make the Ale House a reliably great choice. Their menu items are often made with Milwaukee Brewing Company beer. The pulled pork sandwich is my personal favorite; maybe the best pulled pork sandwich I've ever had. The service is good, and the space is open and pleasant with a great patio overlooking the river. Trivia on Wednesdays is a fun option. You really can't go wrong with the Ale House, especially when there are numerous fantastic Milwaukee beers you'll only find on tap there."
        }
}        